import threading
import time
from multiprocessing import Process

import numpy as np
from numba import jit

from Point import Point


class StarClassifier(Process):
    WHITE = 0.299 * 255 + 0.587 * 255 + 0.114 * 255
    THRESHOLD = 100

    def __init__(self, source, output, corner_type, f_x, f_y, t_x, t_y, pixels_flat, shape, thread_id):
        Process.__init__(self)
        self.source = source
        self.output = output
        self.corner_type = corner_type
        self.f_x = f_x
        self.f_y = f_y
        self.t_x = t_x
        self.t_y = t_y
        self.pixels = np.reshape(pixels_flat, shape)
        self.parent_thread_id = thread_id

    @staticmethod
    @jit()
    def calc_brightness(pixel):
        return 0.299 * pixel[0] + 0.587 * pixel[1] + 0.114 * pixel[2]

    @jit
    def run(self):
        stars = []
        thread_id = threading.get_ident()
        start = time.time()
        for y in range(self.f_y, self.t_y):
            for x in range(self.f_x, self.t_x):
                pixel = self.pixels[y, x]
                new_brightness = round(abs(self.calc_brightness(pixel) - self.WHITE), 2)
                if new_brightness < self.THRESHOLD:
                    if len(stars) <= 0:
                        stars.append([(Point(x, y), new_brightness)])
                    else:
                        found = False
                        for s in stars:
                            for l in s:
                                d_x = abs(l[0].x - x)
                                d_y = abs(l[0].y - y)

                                if d_x == 1 and d_y == 0 or d_x == 0 and d_y == 1 or d_x == 1 and d_y == 1:
                                    s.append((Point(x, y), new_brightness))
                                    found = True
                                    break
                            if found:
                                break
                        if not found:
                            stars.append([(Point(x, y), new_brightness)])
        end = time.time()
        print("image {0} corner processed in thread {1} after {2}s".format(self.source, thread_id, round(end - start, 2)))
        self.output[(self.parent_thread_id, self.corner_type[0])] = stars
        return
