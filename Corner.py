class Corner:
    CORNER_LEFT_TOP = (0, "left top")
    CORNER_RIGHT_TOP = (1, "right top")
    CORNER_LEFT_BOTTOM = (2, "left bottom")
    CORNER_RIGHT_BOTTOM = (3, "right bottom")

    SIZE = 4

    @staticmethod
    def to_corner(val):
        if val == Corner.CORNER_LEFT_TOP[0]:
            return Corner.CORNER_LEFT_TOP
        elif val == Corner.CORNER_RIGHT_TOP[0]:
            return Corner.CORNER_RIGHT_TOP
        elif val == Corner.CORNER_LEFT_BOTTOM[0]:
            return Corner.CORNER_LEFT_BOTTOM
        else:
            return Corner.CORNER_RIGHT_BOTTOM
