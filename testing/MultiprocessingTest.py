import time
from multiprocessing import Process, Queue, Manager

import numpy as np
from PIL import Image
from numba import cuda

from testing.Tester import Tester


def mean(output, order, psa, psb, f_x, t_x, f_y, t_y):
    new_pixels = np.zeros((t_y - f_y, t_x - f_x, 3))
    shape = new_pixels.shape
    for y in range(0, shape[0]):
        for x in range(0, shape[1]):
            # print("y: {0} / x: {1}".format(y, x))
            pa = psa[f_y + y, f_x + x]
            pb = psb[f_y + y, f_x + x]
            p = [(int(int(pa[0]) + int(pb[0])) / 2), (int(int(pa[1]) + int(pb[1])) / 2),
                 (int(int(pa[2]) + int(pb[2])) / 2)]
            new_pixels[y, x] = p[:3]
    output[order] = new_pixels
    print("finished {0}".format(order))


def multiple_processes():
    print('MULTIPLE PROCESSES')
    output = Manager().dict()
    start = time.time()
    image_a = Image.open("testing/starw1.png")
    image_b = Image.open("testing/starw2.png")
    pixels_a = np.array(image_a)
    pixels_b = np.array(image_b)
    width, height = image_a.size

    processes = [Process(target=mean, args=(output, x, pixels_a, pixels_b, 0, width, 0, height)) for x in
                 range(6)]

    for p in processes:
        p.start()

    for p in processes:
        p.join()

    end = time.time()
    print('---------{0}s---------'.format(round(end - start, 2)))
    for k in output.keys():
        print(k)


def single_process():
    print("SINGLE PROCESS")
    output = Manager().dict()
    start = time.time()
    image_a = Image.open("testing/starw1.png")
    image_b = Image.open("testing/starw2.png")
    pixels_a = np.array(image_a)
    pixels_b = np.array(image_b)
    width, height = image_a.size

    [mean(output, x, pixels_a, pixels_b, 0, width, 0, height) for x in range(6)]

    end = time.time()
    print('---------{0}s---------'.format(round(end - start, 2)))
    for k in output.keys():
        print(k)


def test_without_gpu():
    image = Image.open("../raw/DSC9575.JPG")
    pixels = np.array(image)
    width, height = image.size

    start = time.time()
    for y in range(height):
        for x in range(width):
            pixel = pixels[y, x]
    end = time.time()
    print('{0}s'.format(round(end - start, 2)))


@cuda.jit()
def test_gpu(pixels, width, height):
    x, y = cuda.grid(2)
    if x < width and y < height:
        pixel = pixels[y, x]


if __name__ == '__main__':
    image = Image.open("../raw/DSC9575.JPG")
    pixels = np.array(image)
    width, height = image.size

    start = time.time()
    test_gpu(pixels, width, height)
    end = time.time()
    print('{0}s'.format(round(end - start, 2)))
