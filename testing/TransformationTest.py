import numpy as np
from PIL import Image
from Index import find_coeffs

# https://stackoverflow.com/questions/14177744/how-does-perspective-transformation-work-in-pil

image_01 = Image.open("raw/test_01.png")
image_02 = Image.open("raw/test_02.png")
width, height = image_01.size

current = [(6, 6), (width, 6), (width-20, height-20), (6, height)]
new = [(12, 12), (width, 12), (width, height), (12, height)]

alignment_matrix = find_coeffs(new, current)
new_img = image_02.transform((width, height), Image.PERSPECTIVE, alignment_matrix, Image.BICUBIC)


pixels_01 = np.array(image_01)
pixels_02 = np.array(new_img)

image_combined = Image.new('RGB', (width, height), 'white')
pixels_combined = image_combined.load()

for y in range(0, height):
    for x in range(0, width):
        pixel_a = pixels_01[y, x]
        pixel_new = pixels_02[y, x]

        combined_pixel = (int((int(pixel_a[0]) + int(pixel_new[0])) / 2), int((int(pixel_a[1]) + int(pixel_new[1])) / 2), int((int(pixel_a[2]) + int(pixel_new[2])) / 2))
        pixels_combined[x, y] = combined_pixel

image_combined.save("test_result.png")
image_combined.show()
