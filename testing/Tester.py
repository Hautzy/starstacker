from multiprocessing import Process


class Tester:

    def test(self, text):
        print(text)
        ps = [Process(target=self.start_process, args=(i,)) for i in range(2)]

        for p in ps:
            p.start()

    def start_process(self, i):
        print('blablabla {0}'.format(i))