from Point import Point


class Boundary:

    def __init__(self, min_x, min_y, max_x, max_y):
        self.origin = Point(min_x, min_y)
        self.dims = Point(max_x, max_y)
