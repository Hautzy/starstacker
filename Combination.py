import threading
import time
from multiprocessing import Process, Manager
from multiprocessing.sharedctypes import RawArray

import math
import numpy as np
from PIL import Image
from numba import jit, cuda

from Corner import Corner
from Point import Point
from Star import Star
from StarClassifier import StarClassifier
from Workload import Workload


class Combination:
    MARKERS = [(0, 255, 0, 255), (255, 0, 0, 255), (0, 0, 255, 255)]
    A = 0
    B = 1

    def __init__(self, workload_a, workload_b, workload_stack, factor):
        self.workload_stack = workload_stack
        self.workload_a = workload_a
        self.workload_b = workload_b
        self.star_data_a = []
        self.star_data_b = []
        self.factor = factor
        self.main_corner_stars = []
        self.avg_transform_vectors = []
        self.final_image = None
        self.final_pixels = None
        self.final_source = None

    def start(self):
        self.process_image(self.workload_a.source, self.workload_a.image, self.workload_a.pixels, self.workload_a.width, self.workload_a.height,
                           Combination.A)
        self.process_image(self.workload_b.source, self.workload_b.image, self.workload_b.pixels, self.workload_b.width,
                           self.workload_b.height, Combination.B)

        self.main_corner_stars, self.avg_transform_vectors = self.find_transformation_vectors_and_stars()
        transform_points_b, transform_points_a = self.create_new_transform_points()
        self.final_image, self.final_pixels, self.final_source = self.transform_and_stack_images(transform_points_b,
                                                                                                 transform_points_a)
        self.create_new_workload()

    def create_new_workload(self):
        workload = Workload(self.final_source, self.workload_a.workload_stack)
        workload.set_synthetic(self.final_image, self.final_pixels, self.workload_a.width, self.workload_a.height)

    @jit()
    def transform_and_stack_images(self, transform_points_b, transform_points_a):
        start = time.time()
        alignment_matrix = Combination.find_coeffs(transform_points_a, transform_points_b)
        transformed_image = self.workload_b.image.transform((self.workload_a.width, self.workload_a.height),
                                                            Image.PERSPECTIVE, alignment_matrix, Image.BICUBIC)
        pixels_transformed = np.array(transformed_image)

        # Copy the arrays to the device
        A_global_mem = cuda.to_device(self.workload_a.pixels)
        B_global_mem = cuda.to_device(pixels_transformed)

        # Allocate memory on the device for the result
        C_global_mem = cuda.device_array((self.workload_a.height, self.workload_a.width, self.workload_a.pixels.shape[2]))

        # Configure the blocks
        threadsperblock = (16, 16)
        blockspergrid_x = int(math.ceil(self.workload_a.pixels.shape[0] / threadsperblock[0]))
        blockspergrid_y = int(math.ceil(self.workload_a.pixels.shape[1] / threadsperblock[1]))
        blockspergrid = (blockspergrid_x, blockspergrid_y)

        # Start the kernel
        Combination.stack_images[blockspergrid, threadsperblock](A_global_mem, B_global_mem, C_global_mem)

        # Copy the result back to the host
        C = C_global_mem.copy_to_host()
        C = C.astype(np.uint8)

        image_result = Image.fromarray(C)
        final_image_name = self.workload_a.source.replace('.png', '').replace('.jpg', '') + "_" + self.workload_b.source
        image_result.save('res/' + final_image_name)

        end = time.time()
        print('stacking image {1} lasted {0}s'.format(round(end - start, 2), final_image_name))

        return image_result, C, final_image_name

    @staticmethod
    @cuda.jit
    def stack_images(A, B, C):
        row, col = cuda.grid(2)
        if row < C.shape[0] and col < C.shape[1]:
            if B[row, col, 0] == 0 and B[row, col, 1] == 0 and B[row, col, 2] == 0 and C.shape[2] >= 4:
                C[row, col, 0] = A[row, col, 0]
                C[row, col, 1] = A[row, col, 1]
                C[row, col, 2] = A[row, col, 2]
                C[row, col, 3] = 255
            else:
                for i in range(C.shape[2]):
                    C[row, col, i] = (A[row, col, i] + B[row, col, i]) / 2
                if C.shape[2] >= 4:
                    C[row, col, -1] = 255

    def create_new_transform_points(self):
        mcs = self.main_corner_stars
        points_b = [
            (mcs[0][1][1].center.x, mcs[0][1][1].center.y),
            (mcs[1][1][1].center.x, mcs[1][1][1].center.y),
            (mcs[3][1][1].center.x, mcs[3][1][1].center.y),
            (mcs[2][1][1].center.x, mcs[2][1][1].center.y)]
        points_a = [
            (mcs[0][1][0].center.x, mcs[0][1][0].center.y),
            (mcs[1][1][0].center.x, mcs[1][1][0].center.y),
            (mcs[3][1][0].center.x, mcs[3][1][0].center.y),
            (mcs[2][1][0].center.x, mcs[2][1][0].center.y)]
        return points_b, points_a


    def find_transformation_vectors_and_stars(self):
        start = time.time()
        avg_transform_vectors = []
        main_corner_stars = []
        for c in range(0, Corner.SIZE):
            transform_vector_cluster = self.connect_star_data(self.star_data_a[c], self.star_data_b[c])

            star_pairs = self.find_star_pairs(transform_vector_cluster)

            avg_transform_vector = self.calc_avg_transform_vector(star_pairs)
            avg_transform_vectors.append(avg_transform_vector)

            print("{1} + {2}: avg_transform_vec: {0}".format(avg_transform_vector, self.workload_a.source, self.workload_b.source))

            corner = Corner.to_corner(c)
            main_star = self.find_main_corner_star(star_pairs, corner)
            main_corner_stars.append(main_star)
        end = time.time()
        print("{1} + {2}: calculating transformation vectors and stars lasted for {0}s".format(round(end - start, 2), self.workload_a.source, self.workload_b.source))
        return main_corner_stars, avg_transform_vectors

    def find_main_corner_star(self, vector_cluster, corner):
        b = vector_cluster[1][0]
        b_x = b[1][1].center.x
        b_y = b[1][1].center.y

        if corner == Corner.CORNER_LEFT_TOP:
            origin = Point(0, 0)
        elif corner == Corner.CORNER_RIGHT_TOP:
            origin = Point(self.workload_b.width, 0)
        elif corner == Corner.CORNER_LEFT_BOTTOM:
            origin = Point(0, self.workload_b.height)
        else:
            origin = Point(self.workload_b.width, self.workload_b.height)

        b_d = math.pow(b_x - origin.x, 2) + math.pow(b_y - origin.y, 2)

        for s in vector_cluster[1]:
            s_x = s[1][1].center.x
            s_y = s[1][1].center.y

            s_d = math.pow(s_x - origin.x, 2) + math.pow(s_y - origin.y, 2)
            if s_d < b_d:
                b = s
                b_x = b[1][1].center.x
                b_y = b[1][1].center.y
                b_d = math.pow(b_x - origin.x, 2) + math.pow(b_y - origin.y, 2)
        return b

    def process_image(self, source, image, pixels, width, height, type):
        raw_star_data = self.process_image_corners(source, pixels, width, height)
        star_meta_data = self.create_star_meta_data(raw_star_data, source)
        if type == Combination.A:
            self.star_data_a = star_meta_data
        else:
            self.star_data_b = star_meta_data
            self.star_data_b = star_meta_data
        # self.create_marker_image(image, raw_star_data, Combination.MARKERS[type])

    def process_image_corners(self, source, pixels, width, height):
        output = Manager().dict()

        shape = (height, width, len(pixels[0][0]))
        flattened_pixels = RawArray('d', shape[0] * shape[1] * shape[2])
        X_np = np.frombuffer(flattened_pixels).reshape(shape)
        np.copyto(X_np, pixels)

        thread_id = threading.get_ident()

        p1 = StarClassifier(source,
                            output,
                            Corner.CORNER_LEFT_TOP,
                            0,
                            0,
                            int(width * self.factor),
                            int(height * self.factor),
                            flattened_pixels,
                            shape,
                            thread_id)
        p2 = StarClassifier(source,
                            output,
                            Corner.CORNER_RIGHT_TOP,
                            width - int(width * self.factor),
                            0,
                            width,
                            int(height * self.factor),
                            flattened_pixels,
                            shape,
                            thread_id)
        p3 = StarClassifier(source,
                            output,
                            Corner.CORNER_LEFT_BOTTOM,
                            0,
                            height - int(height * self.factor),
                            int(width * self.factor),
                            height,
                            flattened_pixels,
                            shape,
                            thread_id)
        p4 = StarClassifier(source,
                            output,
                            Corner.CORNER_RIGHT_BOTTOM,
                            width - int(width * self.factor),
                            0,
                            width,
                            int(height * self.factor),
                            flattened_pixels,
                            shape,
                            thread_id)

        p1.start()
        p2.start()
        p3.start()
        p4.start()

        p1.join()
        p2.join()
        p3.join()
        p4.join()

        return [
            output[(thread_id, Corner.CORNER_LEFT_TOP[0])],
            output[(thread_id, Corner.CORNER_RIGHT_TOP[0])],
            output[(thread_id, Corner.CORNER_LEFT_BOTTOM[0])],
            output[(thread_id, Corner.CORNER_RIGHT_BOTTOM[0])]]

    def create_star_meta_data(self, raw_star_data, source):
        thread_id = threading.get_ident()
        for c_ind in range(0, Corner.SIZE):
            print(
                "image {0} in thread {1} {3} C: {2} stars".format(source, thread_id, len(raw_star_data[c_ind]), Corner.to_corner(c_ind)[1]))

        star_meta_data = []

        for c_ind in range(0, Corner.SIZE):
            star_meta = self.calc_star_meta_data(raw_star_data[c_ind])
            star_meta.sort(key=lambda e: e.size, reverse=True)
            # print("{0}T: {1} final stars".format(thread_ind, len(star_meta)))
            star_meta_data.append(star_meta)
        return star_meta_data

    def create_marker_image(self, image, raw_star_data, marker):
        thread_id = threading.get_ident()
        start = time.time()
        marker_image = image.copy()

        for c_ind in range(0, Corner.SIZE):
            for s in raw_star_data[c_ind]:
                for p in s:
                    point = p[0]
                    marker_image = self.draw_single_marker(marker_image, (point.x, point.y), marker)

        filename = image.filename.split('/')[-1]
        marker_image.save('res/marker_' + filename)
        end = time.time()
        print("thread {0} finished marker images after {1}s".format(thread_id, round(end - start, 2)))

    @staticmethod
    def draw_single_marker(image, point, marker):
        # TODO: directly change pixel array
        for y in range(point[1], point[1] + 1):
            for x in range(point[0], point[0] + 1):
                image.putpixel((x, y), marker)
        return image

    @staticmethod
    def calc_star_meta_data(stars):
        meta = []
        min_pixel_count = 4
        for s in stars:
            if len(s) > min_pixel_count:
                arr_x = [p[0].x for p in s]
                arr_y = [p[0].y for p in s]
                star_meta = Star.calculate_params(arr_x, arr_y)
                meta.append(star_meta)
        return meta

    @staticmethod
    def calc_avg_transform_vector(cluster):
        if cluster[0] <= 0 and cluster[1] is None:
            return None
        vec = Point(0, 0)
        cluster_size = cluster[0]
        for q in cluster[1]:
            vec.x += q[0].x
            vec.y += q[0].y
        vec = Point(
            int(round(vec.x / cluster_size)), int(round(vec.y / cluster_size)))
        return vec

    @staticmethod
    def connect_star_data(star_meta_data_a, star_meta_data_b):
        clusters = {}
        p = 3

        for i in range(0, len(star_meta_data_a)):
            star_a = star_meta_data_a[i]

            for j in range(0, len(star_meta_data_b)):
                star_b = star_meta_data_b[j]

                t_vec = Point(star_a.center.x - star_b.center.x, star_a.center.y - star_b.center.y)
                # print("{0} vs. {1}, {2}".format(i, j, t_vec))

                found = False
                for c in clusters:
                    if c.x - p <= t_vec.x <= c.x + p and c.y - p <= t_vec.y <= c.y + p:
                        clusters[c].append((t_vec, (star_a, star_b)))
                        found = True
                if not found:
                    clusters[t_vec] = [(t_vec, (star_a, star_b))]
        return clusters

    @staticmethod
    def find_star_pairs(clusters):
        best_vectors = (0, None)
        for vec_cluster in clusters.values():
            if len(vec_cluster) > best_vectors[0]:
                best_vectors = (len(vec_cluster), vec_cluster)
        return best_vectors

    @staticmethod
    def find_coeffs(pa, pb):
        matrix = []
        for p1, p2 in zip(pa, pb):
            matrix.append([p1[0], p1[1], 1, 0, 0, 0, -p2[0] * p1[0], -p2[0] * p1[1]])
            matrix.append([0, 0, 0, p1[0], p1[1], 1, -p2[1] * p1[0], -p2[1] * p1[1]])

        A = np.matrix(matrix, dtype=np.float)
        B = np.array(pb).reshape(8)

        res = np.dot(np.linalg.inv(A.T * A) * A.T, B)
        return np.array(res).reshape(8)
