from Boundary import Boundary
from Point import Point


class Star:
    last_id = 0

    def __init__(self, c_x, c_y, min_x, min_y, max_x, max_y, size):
        self.id = Star.last_id
        Star.last_id += 1
        self.center = Point(c_x, c_y)
        self.boundaries = Boundary(min_x, min_y, max_x, max_y)
        self.size = size

    @classmethod
    def calculate_params(cls, arr_x: [], arr_y: []):
        min_x = min(arr_x)
        min_y = min(arr_y)
        max_x = max(arr_x)
        max_y = max(arr_y)
        c_x = round((max_x - min_x) / 2 + min_x)
        c_y = round((max_y - min_y) / 2 + min_y)
        return Star(c_x, c_y, min_x, min_y, max_x, max_y, len(arr_x))

    def __str__(self):
        return "id: {0}, {1}, s: {2}".format(self.id, self.center, self.size)