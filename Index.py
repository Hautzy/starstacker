import os
import time
from multiprocessing import Process
from threading import Thread

from Combination import Combination
from WorkloadStack import WorkloadStack
from Workload import Workload


def start_combination(workload_a, workload_b):
    combination = Combination(workload_a, workload_b, workload_stack, factor)
    p = Process(target=combination.start())
    processes.append(p)
    p.start()
    p.join()


factor = 0.3
dir_path = os.path.dirname(os.path.realpath(__file__)) + "\\raw"
sources = []
for (dirpath, dirnames, filenames) in os.walk(dir_path):
    sources.extend([name for name in filenames if '.png' in name or '.jpg' in name])
    break
workload_stack = WorkloadStack(start_combination)

'''
todo-list:
* change star detection method
* let the gpu stack the final image
'''

processes = []
input_threads = []

if __name__ == '__main__':
    start = time.time()
    for s in sources:
        workload = Workload(s, workload_stack)
        t = Thread(target=workload.load)
        input_threads.append(t)

    for t in input_threads:
        t.start()

    for t in input_threads:
        t.join()
    end = time.time()

    diff = end-start
    hours = int(int(diff) / 3600)
    diff -= hours * 3600
    minutes = int(int(diff) / 60)
    diff -= minutes * 60
    print('FINISHED AFTER {0}h:{1}min:{2}sec'.format(hours, minutes, round(diff, 2)))