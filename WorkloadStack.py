class WorkloadStack:
    def __init__(self, event_trigger):
        self.items = []
        self.event_trigger = event_trigger

    def is_empty(self):
        return self.items == []

    def push(self, item):
        self.items.append(item)
        print(str(self))
        if self.size() >= 2:
            a = self.pop()
            print('AFTER-POP-' + str(self))
            b = self.pop()
            print('AFTER-POP-' + str(self))
            self.event_trigger(a, b)

    def pop(self):
        return self.items.pop()

    def size(self):
        return len(self.items)

    def __str__(self):
        return 'STACK: count {0}, elements: {1}:'.format(self.size(), [i.source for i in self.items])