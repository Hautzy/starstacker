import threading
import time

import numpy as np
from PIL import Image


class Workload:
    def __init__(self, source, workload_stack):
        self.source = source
        self.image = None
        self.pixels = None
        self.width = 0
        self.height = 0
        self.workload_stack = workload_stack

    def load(self):
        thread_id = threading.get_ident()
        start = time.time()
        self.image = Image.open('raw/' + self.source)
        self.pixels = np.array(self.image)
        self.width, self.height = self.image.size
        end = time.time()
        print("loading thread {0} for image {1} finished after {2}s".format(thread_id, self.source,
                                                                            round(end - start, 2)))
        self.workload_stack.push(self)

    def set_synthetic(self, image, pixels, width, height):
        self.image = image
        self.pixels = pixels
        self.width = width
        self.height = height
        self.workload_stack.push(self)
